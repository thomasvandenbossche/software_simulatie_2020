# Changelog
Alle significante aanpassingen aan de codebase zijn hier te vinden.

## [1.2.3] - 2020-05-15
### Toegevoegd
- passages_in_v4.csv: hoogte schepen toegevoegd
- ship_data_v2: hoogte schepen toegevoegd

## [1.2.2] - 2020-05-15
### Toegevoegd
- passages_in_v3.csv: afmetingen van een schip toegevoegd
- ship_data: lijst met schepen en hun afmetingen

## [1.2.1] - 2020-05-08
### Toegevoegd
- passages_in_v2_csv: elke route van een ship wordt nu voorgesteld door een uniek ShipID.

## [1.2.0] - 2020-04-22
### Toegevoegd
- trajectories_2.geojson: bevat de individuele punten waaruit de trajecten zijn opgebouwd.

### Verwijderd
- passages.csv: kolommen 'DimensionA', 'DimensionB', 'DimensionC', en 'DimensionD' zijn verwijderd.
- passages_in.csv: kolommen 'DimensionA', 'DimensionB', 'DimensionC', en 'DimensionD' zijn verwijderd.

### Aangepast
- map 'trajectory_info': kruistabellen gecorrigeerd.

## [1.1.0] - 2020-04-18
### Toegevoegd
- ship_info.xml: bevat de afmetingen en diepgang die bij elke CEMT klasse horen. 
- Map trajectory_info: bevat kruistabellen die per traject aangeven of schepen kunnen kruisen afhankelijk van hun CEMT klasse.

## [1.0.0] - 2020-03-26
### Toegevoegd
- lock.geojson: "length" en "width" attributen toegevoegd.

### Aangepast
- trajectories.geojson: voor elke trajectory is de koppeling naar de juiste *sectionref* gemaakt.

### Verwijderd
- fairwaysections.geojson: de eenheid km/u is verwijderd bij alle verwijzingen naar snelheden.