import sys

from Project import utils

fairwaysection_dict = {}

# Default dimensions for the visualisation pane
# Settings for the visualization
screen_x_min = 25
screen_x_max = 1000
screen_x_width = screen_x_max - screen_x_min
screen_y_min = 25
screen_y_max = 550
screen_y_width = screen_y_max - screen_y_min

node_x_min = float('inf')
node_x_max = float('-inf')
node_y_min = float('inf')
node_y_max = float('-inf')

# Read the passages
class Vessel(object):
    def __init__(self, shipID, length, width, cemt):
        self.id = shipID
        self.length = length
        self.width = width
        self.cemt = cemt
        self.trajectory_route = []

class Trajectory(object):
    """Defines a trajectory, having four coordinates"""
    def __init__(self, lon1, lat1, trajectoryName):
        self.lon1 = lon1
        self.lat1 = lat1
        self.trajectoryName = trajectoryName

class Node:
    """Defines a nodes of a trajectory"""
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Fairwaysection():
    """Defines a single fairway section"""
    def __init__(self, fw_code, set_of_coordinate_pairs):
        global node_x_min, node_x_max, node_y_min, node_y_max
        self.nodes = []
        self.fw_code = fw_code
        fairwaysection_dict[self.fw_code] = self

        for coordinate_pairs in set_of_coordinate_pairs:
            for coordinates in coordinate_pairs:
                lon = coordinates[0]
                lat = coordinates[1]

                #x, y = utils.convert_and_normalize_coordinates(lon, lat)
                x, y = lon, lat


                node = Node(x, y)
                node_x_min = min(x, node_x_min)
                node_x_max = max(x, node_x_max)
                node_y_min = min(y, node_y_min)
                node_y_max = max(y, node_y_max)

                self.nodes.append(node)


class Bridge():
    """Defines a bridge on a fairway"""
    def __init__(self, fw_code, coordinates_pair):
        self.fw_code = fw_code
        self.x = coordinates_pair[0]
        self.y = coordinates_pair[1]

        # Get the fairway section this belongs to
        if self.fw_code not in fairwaysection_dict:
            print("We have problem here..")

        insertion_idx = -1
        min_distance = float('inf')

        fairwaysection = fairwaysection_dict.get(self.fw_code)
        for idx, fairwaysection_node in enumerate(fairwaysection.nodes):
            distance = utils.haversine([self.y, self.x], [fairwaysection_node.y, fairwaysection_node.x])
            if distance < min_distance:
                min_distance = distance
                insertion_idx = idx

        # Append add the best index
        fairwaysection.nodes.insert(insertion_idx + 1, self)

class Lock():
    """Defines a bridge on a fairway"""

    def __init__(self, fw_code, coordinates_pair):
        self.fw_code = fw_code
        self.x = coordinates_pair[0]
        self.y = coordinates_pair[1]

        # Get the fairway section this belongs to
        if self.fw_code not in fairwaysection_dict:
            print("We have problem here..")

        insertion_idx = -1
        min_distance = float('inf')

        fairwaysection = fairwaysection_dict.get(self.fw_code)
        for idx, fairwaysection_node in enumerate(fairwaysection.nodes):
            distance = utils.haversine([self.y, self.x], [fairwaysection_node.y, fairwaysection_node.x])
            if distance < min_distance:
                min_distance = distance
                insertion_idx = idx

        # Append add the best index
        fairwaysection.nodes.insert(insertion_idx + 1, self)