
# Read the different data files
from Project import IO, data_model, utils
import salabim as sim
import numpy as np
import pandas as pd

vessels_dict = {}
vessels = []

class Vessel(sim.Component):
    """Vessel Salabim component"""
    def process(self):
        # traverse a route (while loop)
        pass

class Vessel_generator(sim.Component):
    """Generate vessels with a given inter-arrival distribution"""

    def process(self):
        print(vessels_dict)
        print(vessels)

        print('test')

def read_data():
    """ Read all the relevant data"""
    global trajectories_dict
    global df_fairway
    global df_bridges
    global df_terminal
    global vessels_dict
    global vessels

    trajectories_dict = IO.read_trajectories()

    # Read the fairway sections
    df_fairway = IO.read_network()
    #print(model.fairwaysection_dict)

    # Read all the bridges in the network
    df_bridges = IO.read_bridges()

    # Read all the locks in the network
    df_locks = IO.read_locks()

    df_terminal = IO.read_terminals()

    vessels, vessels_dict = IO.read_passages()

# Animate
def animate_simulation():
    """Animate the solution"""

    for fairway in df_fairway:
        fairway_arr = np.array([[utils.normalize(node.x,node.y)] for node in fairway.nodes])
        # Make this a one-dimensional array (so we can generate a tuple easily)
        fairway_arr = fairway_arr.ravel()
        # Make a tuple (accepted by the 'AnimatePoints' function
        fairway_tuple = tuple(fairway_arr)
        # Draw all the fairway points on the map
        sim.AnimatePoints(spec=fairway_tuple, text="These are points", textcolor='black')


# Read all data
read_data()

# Simulation settings
env = sim.Environment(trace=True)
env.animation_parameters(width=1920, height=1080, modelname="Alsic Waterway Simulation", animate=True)
env.modelname("Alsic Waterway Simulation")

animate_simulation()

# Generate the vessel
Vessel_generator()

env.run(100)