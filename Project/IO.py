import geopandas as gpd
import pandas as pd
import json

from Project import data_model, utils
from Project.data_model import Vessel, Fairwaysection

trajectories_file = 'project_files/trajectories.geojson'
waterway_file = 'project_files/fairwaysections.geojson'  # All sections in flanders
bridges_file = 'project_files/bridges.geojson'
locks_file = 'project_files/locks.geojson'
terminals_file = 'project_files/terminals.geojson'
passages_file = 'project_files/passages_in.csv'

trajectory_dict = {}

def read_passages():
    """Read the different passages"""
    df_passages = pd.read_csv(passages_file, sep=';')

    # Get all the ships
    df_vessels = df_passages[['ShipID','CEMTKlasse','Width', 'Length']].drop_duplicates(inplace=False)
    df_vessels = df_vessels.reset_index(drop=True)

    # Make a global vessel dict
    global vessel_dict
    vessel_dict = {}
    vessels = []

    # Iterate over the vessels
    for index, row in df_vessels.iterrows():
        # Make a new object

        vessel = Vessel(row['ShipID'], row['Length'], row['Width'], row['CEMTKlasse'])
        vessel_dict[row['ShipID']] = vessel

    # Connect the route
    for index, row in df_passages.iterrows():
        # Get the corresponding vessel
        shipId = row['ShipID']
        trajectoryName = row['TrajectName']

        trajectory = trajectory_dict.get(trajectoryName)
        vessel = vessel_dict.get(shipId)
        vessel.trajectory_route.append(trajectory)

        # Try not to add the trajectory
        vessels.append(vessel)

    # for vessel in vessels:
    #     l = len(vessel.trajectory_route)
    #     for index, obj in enumerate(vessel.trajectory_route):
    #         current_trajectory = vessel.trajectory_route[index]
    #         if index < (l-1):
    #             next_trajectory = vessel.trajectory_route[index + 1]

    return vessels, vessel_dict

def read_trajectories():
    # read the trajectories

    f = open(trajectories_file)

    trajectories_df = gpd.read_file(trajectories_file)
    #print(trajectories_df.head())
    print(trajectories_df['LoLat'])

    global trajectories_dict

    for index,row in trajectories_df.iterrows():
        print(row['LoLat'], row['LoLong'])

        trajectoryName = row['TrajectName']
        if trajectoryName in trajectory_dict:
            # It already exist
            trajectory = trajectory_dict.get(trajectoryName)
            trajectory.lon2 = row['LoLong']
            trajectory.lat2 = row['LoLat']

        else:
            lat1 = row['LoLat']
            lon1 = row['LoLong']
            # It does not exist, make the trajectory
            sectionref = row['sectionref']
            trajectory = data_model.Trajectory(lon1, lat1, trajectoryName)
            trajectory_dict[trajectoryName] = trajectory

    return trajectory_dict

def read_network():
    fairway_sections_list = []
    # read the waterway network

    with open(waterway_file) as f:
        data = json.load(f)

    for feature in data['features']:
        fw_code = feature['properties']['fw_code']
        coordinates = feature['geometry']['coordinates']

        #print('New section:' , feature['properties']['name'])
        #x_norm, y_norm = utils.convert_and_normalize_coordinates()

        fairway_section = data_model.Fairwaysection(fw_code, coordinates)
        fairway_sections_list.append(fairway_section)

    return fairway_sections_list

def read_bridges():
    # read the waterway network
    bridges_df = gpd.read_file(bridges_file)

    with open(bridges_file) as f:
        data = json.load(f)

    for feature in data['features']:
        #print(feature)
        fw_code = feature['properties']['fw_code']
        bridge = data_model.Bridge(fw_code, feature['geometry']['coordinates'])

    return bridges_df

def read_locks():
    # read the waterway network
    locks_df = gpd.read_file(locks_file)

    with open(locks_file) as f:
        data = json.load(f)

    for feature in data['features']:
        #print(feature)
        fw_code = feature['properties']['sectionref']
        lock = data_model.Lock(fw_code, feature['geometry']['coordinates'])

    return locks_df

def read_terminals():
    # read the waterway network
    terminals_df = gpd.read_file(terminals_file)
    return terminals_df